projectile={}
projectiles={}

-- create a projectile
function projectile:new(x, y, sp, w, h)
    local o={}
    setmetatable(o, self)
    self.__index = self
    o.x=x
    o.y=y
    o.w=w or 1
    o.h=h or 1
    o.dx=0
    o.dy=0
    o.spr=sp
    o.speed=2
    o.frms=2
    o.isfaceright=true
    o.bounce=true --do we turn around at a wall?
    o.projectile=true
    o.age=0
    o.maxage=60
    return o
end

function projectile:draw()
    if self.isfaceright then
        anim(self, self.spr, self.frms, 6, true)
    else
        anim(self, self.spr, self.frms, 6, false)
    end
end

function projectile:move()
    -- if (true) return
    self.startx = self.x
    if self.isfaceright then
        self.dx = self.speed
    else
        self.dx = -self.speed
    end
    update_loc(self)
end

function projectile:update()
    self.age += 1
    if self.age > self.maxage then
        del(projectiles, self)
    end
end

function projectile:collide(actor)
    if actorcollide(self,actor) then
        if actor.bad then
            sfx(3)
            del(baddies, actor)
            del(projectiles, self)
        end
    end
end

function spawn_projectile(plyrx)
    local proj = projectile:new(plyrx.x, plyrx.y, 18)
    proj.isfaceright = plyrx.isfaceright
    add(projectiles, proj)
end
