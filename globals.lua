--globals
globals=
{
    grav=0.2, -- gravity per frame
    -- debug=true,
    debug=false,
    test=false,
    -- gradient=false,
    gradient=true,
    gameover=false,
    victory=false,
    bg_top=true,
    bg_bottom=true,
    --bg_start=false,
    bg_start=true,
    cell_width=8,
    music=1, -- -1 for off, 1 for first set
    mapwidth=64,
    mapheight=16, -- this is unused at present
    player_spawn_x=32,
    -- player_spawn_x=648, -- for testing goal conditions
    player_spawn_y=40,
    gameready=false,
}

_min_mem = 9999
_max_mem = 0

function globals:reset()
    globals.gameover = false
    globals.victory = false
    resetbaddies()
    resetpickups()
    player1 = player:new(globals.player_spawn_x,globals.player_spawn_y)
    projectiles = {}
    globals.gameready = true
end
