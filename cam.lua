cam={}

--create a new camera
function cam:new(mapwidth)
    local o={}
    setmetatable(o, self)
    self.__index=self
    o.x=0
    o.mapwidth=mapwidth
    return o
end

-- move our camera to center on the player
-- todo: follow y location too
function cam:followplayer(playerx, playery)
    -- self.x = max(playerx - 64.0, 0)
    self.x = playerx - 64.0
    -- use this to set an end limit on pos
    -- self.x = min(self.x, (self.mapwidth - 16) * 8)
    -- self.y = max(playery + 64.0, 0)
    self.y = playery + 64.0
    camera(self.x, 0)
end

function cam:getx()
    return self.x
end

function cam:gety()
    return self.y
end

function cam:reset()
    camera()
end
