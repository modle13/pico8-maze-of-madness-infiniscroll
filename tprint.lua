-- tprint credit goes to https://www.lexaloffle.com/bbs/?pid=77892

function init_tprint()
    local font,j="10011730c0337d5f337eb34889366aa103222e21d1355d5311c4211020841103088833e3e343f234ab932eb13754c326b7326ae30cb932eaa33aa210a21502144214a208a20f535a4c23da23df225e23fe22de215e23d63785f11d23b03499f11f378de3705e37a5e21de23ce205e2356225f37a0e33b0e37b1e3499233b1622da347ff3208237ff13082222102041378be32abf223f33a3f22bf20bf3762e37c9f347f121f136c9f221f37cdf3783f33a2e308bf37b2e368bf21b6307e137e0f33f0f37d9f36c9b31f87227d345c411b311d13046237fff51ffffff5155555550fd7b4f50ecc72e51502815502f39e850eeffee5077f9e7504745c4504fb7e4504f7fc450e8c76e51fadebf5010ff1850e8d62e50477dc45040100450edc62e504e3b84511dff7150e9c66e508202085041110450eaeeae515ad6b551f07c1f",1
    tprint_bmp,tprint_w={},{}
    for i=0,121 do
        local w=tonum(sub(font,j,j))
        local bmp="00"..sub(font,j+1,j+ceil(w*1.25))
        tprint_w[i+32]=w
        tprint_bmp[i+32]=tonum("0x"..sub(bmp,-7,-5).."."..sub(bmp,-4))
        j+=ceil(w*1.25)+1
    end
end

function tprint(s,x,y,c,d)
    if (not c) c=color()
    color(c)
    local camx,camy=peek2(0x5f28),peek2(0x5f2a)
    if (x-camx<-128 or x-camx>128) return

    local xx,yy=x,y-1
    for i=1,#s do
        local ch=sub(s,i,i)
        if ch=="\n" then
            yy+=6
            xx=x
        else
            local cs=tprint_bmp[ord(ch)] or 0x0.7fff
            repeat
                if xx-camx>=0 and xx-camx<=127 then
                    for rw=1,5 do
                        if cs>>>rw-1&0x0.0001>0 then
                            if (d) rectfill(xx,yy+rw,xx+1,yy+rw+1,d)
                            pset(xx,yy+rw,c)
                        end
                    end
                end
                cs>>>=5
                xx+=1
            until cs==0
            xx+=1
        end
    end
end

function ctprint(s,x,y,c,d)
    tprint(s,x-tprint_width(s)/2,y,c,d)
end

function tprint_width(s)
    local w=#s-1
    for i=1,#s do
        w+=tprint_w[ord(sub(s,i,i))] or 3
    end
    return w
end
