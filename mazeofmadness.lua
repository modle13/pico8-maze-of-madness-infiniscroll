function _init()
    globals:reset()
    mycam = cam:new(globals.mapwidth)
    player1 = player:new(globals.player_spawn_x,globals.player_spawn_y)
    player1.x = 64
    -- player1.x = 142
    -- player1.y = 10
    init_tprint()
    init_gradient()
    music(globals.music)
    total_score = 0
end

function _update()
    if (not globals.gameready) return
    player1:move()
    player1:update()
    replace_map_sprites()
    player1.score = total_score + player1.x - 64
    wrap_map()
    checkwallcollision(player1)
    -- spawn_baddies(player1:getx())
    for i,actor in pairs(baddies) do
        actor:move()
        checkwallcollision(actor)
        player1:collide(actor)
    end
    for i,actor in pairs(projectiles) do
        actor:update()
        actor:move()
        checkwallcollision(actor)
        for i,baddie in pairs(baddies) do
            actor:collide(baddie)
        end
    end
end

function replace_map_sprites()
    for y = 0,15 do
        back_pos = (player1.x / 8) - 10
        front_pos = (player1.x / 8) + 16
        mset(back_pos, y, 0)
        mset(front_pos, y, 0)
        -- 20% chance to place block
        if (rnd(100) < 20) mset(front_pos, y, 50)
    end
end

function wrap_map()
    -- replace map cells 0 through 16 with current view
    if player1.x > 256 then
        for y = 0,15 do
            for x = 0,16 do
                -- the -8 is to shift from center of view, the player's x pos
                target_sprite = mget((player1.x / 8) + x - 8, y)
                mset(x, y, target_sprite)
            end
        end
        total_score += player1.x - 64
        player1.x = 64
    end
end

function _draw()
    --clear the screen
    cls()

    if globals.gameover or globals.victory then
        handle_end_screen()
        return
    end

    mycam:followplayer(player1:getx(), player1:gety())

    -- map API
    -- map( celx, cely, sx, sy, celw, celh, [layer] )
    -- celx, cely: start position on cel map to draw
    -- sx  , xy  : x and y coords of screen to place in topleft corner
    -- celw, celh: are the number of map cells wide and high to draw

    -- background maps; mycam:getx() / # gives the sliding/perspective effect
    -- starting at 16,16 of map sheet,
    --      pin to (3/4x position of cam),16
    --          (16 is 16 pixels down frop top, or 2 cells)
    --      draw 128x8 cells
    --          or sprites 16-143 across, 16-23 high
    if globals.bg_top then
       -- map(16, 16, mycam:getx() / 1.5, 16, 128, 8)
    end

    -- starting at 16,24 of map sheet,
    --      pin to (1/2x position of cam),64
    --          (64 is halfway down frop top of screen)
    --      draw 38x8
    --          or sprites 16-53 across, 24-31 high
    if globals.bg_bottom then
       -- map(16, 24, mycam:getx() / 2,   64, 128,  8)
    end

    -- static starting area?
    -- starting at 0,16 of map sheet,
    --      pin to 0,0
    --          (top left of screen)
    --      draw 16x16 cells
    --          or sprites 0-15 across, 16-31 high
    if globals.bg_start then
        -- map(0,  16, 0,             0,   16, 16)
    end

    -- foreground map/solid tiles
    -- starting at 0,0 of map sheet,
    --      pin to 0,0
    --          this is in pixels, so 8 * sprite_num
    --          8 = 64, 16 = 128
    --          screen is 128x128 pixels
    --          0, 0 is top left of screen
    --      draw 256x128
    --          or sprites 0-255 across, 0-127 high
    --      but only if they match the provided bitfield
    --          If specified, only draw sprites that have flags set
    --          for every bit in this value (a bitfield).
    --          The default is 0 (draw all sprites).
    --          0x7 will draw sprites with the first flag set

    -- x1
    map(0,  0,  0,      0,  16, 16, 0xf)
    -- x2
    map(16, 0, 16 * 8,  0,  16, 16, 0xf)
    -- x3
    map(32, 0, 16 * 16, 0,  16, 16, 0xf)
    -- x4
    map(48, 0, 16 * 24, 0,  16, 16, 0xf)

    for i, actor in pairs(baddies) do
        actor:draw()
    end
    for i, actor in pairs(projectiles) do
        actor:draw()
    end

    printh(player1.x)
    player1:draw()

    -- drawing before mycam:reset makes text stationary
    -- draw_story_text()

    mycam:reset()

    -- drawing after mycam:reset makes text follow the cam
    player1:printscore()
    --print(globals.test,0,0)
    player1:drawlives()
    if(globals.debug) draw_debug()
end

function handle_end_screen()
    if globals.gameover then
        map(112, 16, 0, 0, 16, 16)
        print('game over', 28, 24)
        print('distance', 40, 42)
        print(player1.score, 40, 60)
        print('respawn', 40, 80)
    end

    if globals.victory then
        map(96, 16, 0, 0, 16, 16)
        print('you win!', 28, 24)
        print('you found: ' .. player1.score, 12, 42)
        print("of shockbunny's", 4, 50)
        print("snacks", 44, 58)
        print('play again', 40, 72)
    end
    if (btn(5)) globals:reset()
end
