-- object, starting frame, number of frames,
-- animation speed, flip
function anim(actor, sf, nf, sp, fl)
    -- fl is flip
    if (not actor.a_ct) actor.a_ct = 0
    if (not actor.a_st) actor.a_st = 0
    -- actor.a_ct += 1
    actor.a_ct += actor.w
    if (actor.a_ct % (30 / sp) == 0) then
        -- st is start?
        actor.a_st += 1
        if (actor.a_st == nf) actor.a_st = 0
    end
    actor.a_fr = sf + actor.a_st * actor.w
    -- subtract `(height - 1) * 16` because want to handle collisions with bottom sprite
    -- but spr draws from top left
    -- (rows are 16 sprites long)
    spr(actor.a_fr - (actor.h - 1) * 16, actor.x, actor.y - (actor.h - 1) * 8, actor.w, actor.h, fl)
end
