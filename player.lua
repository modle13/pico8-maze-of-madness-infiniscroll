player={}

function player:new(x,y)
    local o={}
    setmetatable(o, self)
    self.__index = self
    o.x=x
    o.y=y
    o.w=1
    o.h=1
    o.spawnx=x
    o.spawny=y
    o.dx=0
    o.dy=0
    o.isgrounded=false
    o.jumpvel=2
    o.isfaceright=true
    o.jumptimer=0 --jump button timer
    o.jumppressed=false --prevent double jumps
    o.score=0
    o.bounce=false --do we turn around at a wall?
    o.bad=false
    o.start_lives=3
    o.lives=o.start_lives
    o.invuln=true
    o.invtimer=20
    o.shootready=false
    o.shoottimer=10
    o.flash=false
    return o
end

function player:drawlives()
    for i=1, self.lives do
        spr(001,128-(10*i),0)
    end
end

function player:reset()
    self.lives = self.start_lives
    self.x, self.y = self.spawnx, self.spawny
    self.score = 0
end

function player:jump()
    self.dy=-self.jumpvel
    self.jumptimer+=1
    sfx(000)
end

function player:extendjump()
    self.dy=-self.jumpvel
    self.jumptimer+=1
end

function player:moveleft()
    self.isfaceright = false
    self.dx=-2
end

function player:moveright()
    self.isfaceright = true
    self.dx=2
end

function player:moveup()
    self.dy=-2
end

function player:movedown()
    self.dy=2
end

function player:move()
    if (not globals.gameready) return

    map_cell = mget(self.x / 8, self.y / 8)
    -- map cell flag 3 is a doorway
    if (fget(map_cell, 3)) globals.victory = true globals.gameready = false return

    --remember where we started
    self.startx=self.x
    self.starty=self.y

    --if player is on the ground start a jump
    if btn(4)
        and self.isgrounded
        and self.jumptimer==0 then
        self:jump()
        self.jumppressed = true
    --if holding down make jump higher
    elseif btn(4)
        and self.jumptimer<10
        and self.jumppressed
        and self.dy < 0 then
        self:extendjump()
    elseif not btn(4) then
        self.jumppressed = false
    end

    self.dx=0
    if btn(0) then --left
        self:moveleft()
    end
    if btn(1) then --right
        self:moveright()
    end
    if btn(2) then --up
        --self:moveup()
    end
    if btn(3) then --down
        --self:movedown()
    end
    update_loc(self)
    if self.y > 128 then
        -- play death animation like in ninja cat
        self:loselife()
        -- self.x = self.spawnx
        self.y = self.spawny
        sfx(2)
    end
end

function player:draw()
    -- only draw the player on a non-flash frame
    if self.flash==false then
        --draw player sprite
        if not self.isgrounded then
            if self.isfaceright then
                spr(6,self.x,self.y,1,1,false)
            else
                spr(6,self.x,self.y,1,1,true)
            end
        elseif self.dx==0 then
            if self.isfaceright then
                spr(1,self.x,self.y,1,1,false)
            else
                spr(1,self.x,self.y,1,1,true)
            end
        elseif self.dx>0 then
            anim(self,1,5,10,false)
        else
            anim(self,1,5,10,true)
        end
    end

    -- flash the player while
    -- invulnerable
    if self.invuln==true then
        if self.flash==true then
            self.flash=false
        else
            self.flash=true
        end
    else
        self.flash=false
    end
end

function player:getx()
    return self.x
end

function player:gety()
    return self.y
end

function player:printscore()
    -- print("snacks:" .. self.score, 0, 2, 7)
    print(self.score .. "m", 0, 2, 7)
end

function player:collide(actor)
    if actorcollide(self,actor)
        and not self.invuln then
        sfx(3)
        self:loselife()
    end
end

function player:loselife()
    self.lives -= 1
    self.invuln = true
    self.invtimer = 100
    if self.lives <= 0 then
        globals.gameover = true
        globals.gameready = false
    end
end

function player:update()
    self.invtimer -= 1
    if self.invtimer <= 0 then
        self.invuln = false
    end
    self.shoottimer -= 1
    if self.shoottimer <= 0 then
        self.shootready = true
    end
    if btn(5) and self.score > 0 and self.shootready then
        self.shoot(self)
    end
end

function player:shoot()
    spawn_projectile(self)
    self.score -= 1
    -- 15 frames is half of a second
    self.shoottimer = 15
    self.shootready = false
end
