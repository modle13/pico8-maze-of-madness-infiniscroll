function count_flags(num)
    local count=0
    for y=0,16 do
        for x=0,64 do
            val = mget(x,y)
            if fget(val,num) then
                count+=1
            end
        end
    end
    return count
end
