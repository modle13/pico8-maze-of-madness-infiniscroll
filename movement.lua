function update_loc(actor)
    -- moveleft/right
    actor.x += actor.dx
    -- accumulate gravity
    actor.dy += globals.grav
    -- fall
    -- TODO: set max speed here
    actor.y += actor.dy
end

function intersect(min1, max1, min2, max2)
  return max(min1,max1) > min(min2,max2) and
         min(min1,max1) < max(min2,max2)
end

function actorcollide(actor1,actor2)
    -- return intersect(actor1.x, actor1.x+8,
    --    actor2.x, actor2.x+8) and
    -- intersect(actor1.y, actor1.y+8,
    --    actor2.y, actor2.y+8)
    return intersect(actor1.x, actor1.x+8*actor1.w,
        actor2.x, actor2.x+8*actor2.w) and
    intersect(actor1.y, actor1.y+8*actor1.h,
        actor2.y, actor2.y+8*actor2.h)
end

function checkwallcollision(actor)
    --check for walls in the
    --direction we are moving.
    local x_offset = 0
    if actor.dx > 0 then x_offset = 7 + 8 * (actor.w - 1) end

    --TODO: also check height
    --look for a wall
    collided = false
    -- check each block from sprite base to sprite height
    -- this will be a single iteration for 1-cell tall sprites
    for check_y=0,actor.h-1 do
        if not collided then
            local vector1 = mget((actor.x + x_offset) / 8, (actor.y + 7) / 8)
            local vector2 = mget((actor.x + x_offset) / 8, (actor.y - 8 * check_y) / 8)
            collided = check_vector_collision(actor, vector1, vector2, x_offset, check_y)
        end
    end

    --check bottom corners of object
    local vertex1 = mget((actor.x) / 8, (actor.y + 8) / 8)
    local vertex2 = mget((actor.x + 7) / 8, (actor.y + 8) / 8)

    --assume they are floating
    --until we determine otherwise
    actor.isgrounded = false

    --only check for floors when
    --moving downward
    if actor.dy >= 0 then
        --look for a solid tile
        if fget(vertex1, 0) or fget(vertex2, 0) then
            --place actor on top of tile
            actor.y = flr((actor.y) / 8) * 8
            --halt velocity
            actor.dy = 0
            --allow jumping again
            actor.isgrounded = true
            actor.jumptimer = 0
        end
    end

    --hit ceiling
    --check top corners
    vertex1 = mget((actor.x) / 8, (actor.y) / 8)
    vertex2 = mget((actor.x + 7) / 8, (actor.y) / 8)

    --only check for ceilings when
    --moving up
    --if actor.dy<0 then
        --look for solid tile
        if fget(vertex1, 0) or fget(vertex2, 0) then
            --position p1 right below
            --ceiling
            actor.y = flr((actor.y + 8) / 8) * 8
            --halt upward velocity
            actor.dy = 0
            actor.x = actor.startx
        end
    --end

    -- check if we're touching a pickup
    local check_x = (actor.x + x_offset) / 8
    local check_y = (actor.y + 7) / 8
    local h = mget(check_x, check_y)
    -- this actor.bad/actor.projectile check is so those actors can't interact with pickups
    if fget(h, 1) and not actor.bad and not actor.projectile then
        -- actor.score += 1
        sfx(1)
        -- this deletes the sprite by setting the map sprite
        -- to something that does not exist
        -- mset(celx, cely, spritenum)
        -- TODO: add sprite data to a table for reloads on gameover/win
        add(pickupspawns, check_x .. ',' .. check_y .. ',' .. h)
        mset(check_x, check_y, 000)
    end
end

function check_vector_collision(actor, vector1, vector2, x_offset, y_offset)
    if fget(vector1, 0) or fget(vector2, 0) then
        actor.x = actor.startx
        -- turn bouncers around
        if actor.bounce then
            -- turn around if cell at position 'behind' sprite is not solid
            -- actor.x - x_offset is a position directly to the left
            --      because x_offset is either 0 (moving left) or actor.width - 1 px (moving right)
            -- actor.y + 7 is a position below
            --      and y_offset is number of cells high, not px
            --      really should check all 3 heights here for multi-cell sprites
            --      but this will do for now
            local h2 = mget((actor.x - x_offset) / 8, (actor.y + 7) / 8 - y_offset)
            if not fget(h2, 0) then
                if actor.isfaceright then
                    actor.isfaceright = false
                else
                    actor.isfaceright = true
                end
            end
        end
        return true
    end
    return false
end
